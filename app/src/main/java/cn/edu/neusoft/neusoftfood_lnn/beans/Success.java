package cn.edu.neusoft.neusoftfood_lnn.beans;

public class Success {
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
