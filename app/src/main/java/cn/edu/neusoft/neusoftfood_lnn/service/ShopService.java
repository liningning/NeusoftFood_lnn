package cn.edu.neusoft.neusoftfood_lnn.service;

import java.util.List;

import cn.edu.neusoft.neusoftfood_lnn.beans.Shop;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ShopService {
    @GET("getAllShops.do")
    Call<List<Shop>> getAllShops();
}
