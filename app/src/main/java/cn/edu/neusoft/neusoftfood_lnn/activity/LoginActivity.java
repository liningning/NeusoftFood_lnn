package cn.edu.neusoft.neusoftfood_lnn.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cn.edu.neusoft.neusoftfood_lnn.R;
import cn.edu.neusoft.neusoftfood_lnn.beans.UserInfo;
import cn.edu.neusoft.neusoftfood_lnn.listener.RetrofitListener;
import cn.edu.neusoft.neusoftfood_lnn.model.UserModel;

public class LoginActivity extends BaseActivity implements RetrofitListener<UserInfo> {
    private EditText etname,etpass;
    private Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews()
    {
        etname=findViewById(R.id.etname);
        etpass=findViewById(R.id.etpass);
        btnlogin=findViewById(R.id.btnlogin);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserModel userModel = new UserModel();
                userModel.userLogin(etname.getText().toString().trim(),
                        etpass.getText().toString().trim(),LoginActivity.this);
            }
        });
    }

    @Override
    public void onSuccess(UserInfo userInfo, int flag) {
        if(!userInfo.getUserid().equals("0"))
            showToast(LoginActivity.this,"登录成功");
        else
            Toast.makeText(LoginActivity.this, "用户名或密码错误", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onFail() {
        Toast.makeText(LoginActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
    }
}
