package cn.edu.neusoft.neusoftfood_lnn.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cn.edu.neusoft.neusoftfood_lnn.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
