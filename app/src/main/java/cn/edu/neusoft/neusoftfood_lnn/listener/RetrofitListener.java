package cn.edu.neusoft.neusoftfood_lnn.listener;

public interface RetrofitListener<T> {
    public void onSuccess(T t, int flag);
    public void onFail();
}

