package cn.edu.neusoft.neusoftfood_lnn.model;

import cn.edu.neusoft.neusoftfood_lnn.beans.UserInfo;
import cn.edu.neusoft.neusoftfood_lnn.common.Constants;
import cn.edu.neusoft.neusoftfood_lnn.listener.RetrofitListener;
import cn.edu.neusoft.neusoftfood_lnn.service.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserModel {
    private UserService service;
    private Retrofit retrofit;

    public UserModel() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(UserService.class);
    }
    public void userLogin(String username, String password, final RetrofitListener listener)  {
        Call call=service.login(username, password);
        Callback<UserInfo> callback = new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                listener.onSuccess(response.body(),1);
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                listener.onFail();
            }
        };
        call.enqueue(callback);
    }
}

