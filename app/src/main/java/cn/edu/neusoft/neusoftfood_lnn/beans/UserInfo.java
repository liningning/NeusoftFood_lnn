package cn.edu.neusoft.neusoftfood_lnn.beans;

public class UserInfo {
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
