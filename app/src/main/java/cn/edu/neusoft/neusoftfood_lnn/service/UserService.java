package cn.edu.neusoft.neusoftfood_lnn.service;

import cn.edu.neusoft.neusoftfood_lnn.beans.Success;
import cn.edu.neusoft.neusoftfood_lnn.beans.UserInfo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {

    @GET("userLogin.do")
    Call<UserInfo> login(@Query("username")String username,
                         @Query("userpass")String password);


    @GET("userRegister.do")
    Call<Success> register(
            @Query("username") String username,
            @Query("userpass") String userpass,
            @Query("mobilenum") String mobilenum,
            @Query("address") String address,
            @Query("comment") String comment
    );
}
